import { colors } from "./../../../src/modules/game/entities/IColors";
import { Connection, createConnection, getRepository, Repository } from "typeorm";
import request from "supertest";
import app from "../../../src/shared/infra/http/app";
import { Game } from "../../../src/modules/game/infra/typeorm/entity";

let connection: Connection;
let gameRepository: Repository<Game>;
let gameId: number;
let gameSize: number;
let gameColors: number;

describe("Make game move ", () => {
    beforeAll(async () => {
        connection = await createConnection();
        gameRepository = getRepository(Game);
    });
    beforeEach(async () => {
        const mockGame = {
            size: 4,
            colors: 3,
        };
        const response = await request(app).post("/game").send(mockGame);
        gameId = response.body.id;
        gameSize = response.body.size;
        gameColors = response.body.colors;
    });

    afterEach(async () => {
        await connection.query("DELETE FROM game");
    });
    afterAll(async () => {
        await connection.close();
    });

    it("should fail when other parameters are sent in body", async () => {
        const mockFailGame = {
            size: 4,
            moves: ["blue", "yellow"],
        };
        const response = await request(app).post(`/game/${gameId}`).send(mockFailGame);
        expect(response.status).toBe(400);
        expect(response.body.validation.body.message).toMatch('"size" is not allowed');
    });
    it("should fail when make move is empty", async () => {
        const mockFailGame = {
            moves: [],
        };
        const response = await request(app).post(`/game/${gameId}`).send(mockFailGame);
        expect(response.status).toBe(400);
        expect(response.body.validation.body.message).toMatch('"moves" does not contain 1 required value(s)');
    });

    it("should pass when make move is valid", async () => {
        const mockSuccessGame = {
            moves: ["blue"],
        };
        const response = await request(app).post(`/game/${gameId}`).send(mockSuccessGame);
        expect(response.status).toBe(200);
        expect(response.body.moves).toStrictEqual(mockSuccessGame.moves);
    });
});
