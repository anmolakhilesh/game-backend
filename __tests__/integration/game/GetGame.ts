import { Connection, createConnection, getRepository, Repository } from "typeorm";
import request from "supertest";
import app from "../../../src/shared/infra/http/app";
import { Game } from "../../../src/modules/game/infra/typeorm/entity";

let connection: Connection;
let gameRepository: Repository<Game>;

describe("Get game", () => {
    beforeAll(async () => {
        connection = await createConnection();
        gameRepository = getRepository(Game);
    });

    afterEach(async () => {
        await connection.query("DELETE FROM game");
    });
    afterAll(async () => {
        await connection.close();
    });

    it("should get game by id", async () => {
        const createdGame = await gameRepository.save(
            gameRepository.create({
                size: 4,
                colors: 3,
                moves: [],
                optimalMoves: [],
                board: {},
            }),
        );
        const response = await request(app).get(`/game/${createdGame.id}`);
        expect(response.status).toBe(200);
    });

    it("should fail when wrong game id is given", async () => {
        const response = await request(app).get(`/game/99`);
        expect(response.status).toBe(404);
        expect(response.body.message).toMatch("Game not found");
    });
});
