import { Connection, createConnection, getRepository, Repository } from "typeorm";
import request from "supertest";
import app from "../../../src/shared/infra/http/app";
import { Game } from "../../../src/modules/game/infra/typeorm/entity";

let connection: Connection;
let gameRepository: Repository<Game>;

describe("Get games", () => {
    beforeAll(async () => {
        connection = await createConnection();
        gameRepository = getRepository(Game);
    });

    afterEach(async () => {
        await connection.query("DELETE FROM game");
    });
    afterAll(async () => {
        await connection.close();
    });

    it("should have games after creation", async () => {
        await gameRepository.save(
            gameRepository.create({
                size: 6,
                colors: 9,
                moves: [],
                optimalMoves: [],
                board: {},
            }),
        );
        const response = await request(app).get("/game");
        expect(response.status).toBe(200);
        expect(response.body).toHaveLength(1);
    });
});
