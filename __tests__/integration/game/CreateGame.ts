import { Connection, createConnection } from "typeorm";
import request from "supertest";
import app from "../../../src/shared/infra/http/app";

let connection: Connection;

describe("Create game", () => {
    beforeAll(async () => {
        connection = await createConnection();
    });
    afterEach(async () => {
        await connection.query("DELETE FROM game");
    });
    afterAll(async () => {
        await connection.close();
    });

    it("should not be able to create a game with size more than 10", async () => {
        const mockFailGame = {
            size: 11,
            colors: 3,
        };
        const response = await request(app).post("/game").send(mockFailGame);

        expect(response.status).toBe(400);
        expect(response.body.validation.body.message).toMatch('"size" must be less than or equal to 10');
    });

    it("should not be able to create a game with colors more than 9", async () => {
        const mockFailGame = {
            size: 3,
            colors: 10,
        };
        const response = await request(app).post("/game").send(mockFailGame);

        expect(response.status).toBe(400);
        expect(response.body.validation.body.message).toMatch('"colors" must be less than or equal to 9');
    });

    it("should be able to create a game", async () => {
        const mockSuccessGame = {
            size: 4,
            colors: 3,
        };
        const response = await request(app).post("/game").send(mockSuccessGame);

        expect(response.status).toBe(201);
        expect(response.body.size).toBe(mockSuccessGame.size);
        expect(response.body.colors).toBe(mockSuccessGame.colors);
    });
});
