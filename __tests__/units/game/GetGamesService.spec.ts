import "reflect-metadata"; // TODO move to
import { FakeGameRepository } from "../../../src/modules/game/repositories/FakeGameRepository";
import { GetAllGamesQuery } from "../../../src/modules/game/services/queries/GetAllGamesQuery";

let fakeGameRepository: FakeGameRepository;

let getAllGames: GetAllGamesQuery;

describe("Get Reviews Service", () => {
    beforeEach(() => {
        fakeGameRepository = new FakeGameRepository();
        getAllGames = new GetAllGamesQuery(fakeGameRepository);
    });
    it("should be able to get all reviews", async () => {
        await fakeGameRepository.create({
            size: 4,
            colors: 5,
        });
        await fakeGameRepository.create({
            size: 3,
            colors: 6,
        });
        await expect(getAllGames.execute()).resolves.toHaveLength(2);
    });
});
