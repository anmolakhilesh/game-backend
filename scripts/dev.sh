#!/bin/sh

./scripts/wait-for-it.sh $POSTGRES_HOST:5432 --timeout=30 --strict -- echo "yo... postgres up and running"
yarn migration:run
yarn dev