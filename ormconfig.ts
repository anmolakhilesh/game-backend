import { ConnectionOptions } from "typeorm";

const config: ConnectionOptions = {
    name: "default",
    type: "postgres",
    // host: process.env.POSTGRES_HOST,
    host: "localhost",
    port: Number(process.env.POSTGRES_PORT),
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    migrationsRun: true,
    synchronize: true,
    entities: ["./src/modules/game/infra/typeorm/entity/*.ts"],
    migrations: ["./src/shared/infra/typeorm/migrations"],
    cli: {
        entitiesDir: "./src/modules/game/infra/typeorm/entity/*.ts",
        migrationsDir: "./src/shared/infra/typeorm/migrations",
    },
};

export = config;
