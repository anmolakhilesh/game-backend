import { Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn, Entity } from "typeorm";
import { IGame } from "../../../entities/IGame";

@Entity({ name: "game" })
export class Game implements IGame {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: "size" })
    size: number;

    @Column({ name: "colors" })
    colors: number;

    @Column({ name: "moves", type: "text", array: true })
    moves: string[];

    @Column({ name: "optimal_moves", type: "text", array: true })
    optimalMoves: string[];

    @Column({ name: "board", type: "jsonb" })
    board: object;

    @CreateDateColumn({ name: "created_at" })
    createdAt: Date;

    @UpdateDateColumn({ name: "updated_at" })
    updatedAt: Date;
}
