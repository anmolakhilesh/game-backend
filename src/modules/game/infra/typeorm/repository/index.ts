import { getRepository, Repository } from "typeorm";
import AppError from "../../../../../shared/errors/AppError";
import { CreateGameDTO } from "../../../dtos/CreateGameDTO";
import { MakeMoveDTO } from "../../../dtos/MakeMoveDTO";
import { initializeBoard } from "./../../../controllers/initializeBoard";
import { optimalMoves } from "./../../../controllers/optimalMoves";
import { IGameRepository } from "../../../repositories/IGameRepository";
import { Game } from "../entity";
import { IMove } from "../../../entities/IMove";

class GameRepository implements IGameRepository {
    private ormRepository: Repository<Game>;
    constructor() {
        this.ormRepository = getRepository(Game);
    }

    public async findById(id: number): Promise<Game> {
        const game = await this.ormRepository.findOne({ id });
        if (!game) {
            throw new AppError("Game not found", 404);
        }
        return game;
    }

    public async findAll(): Promise<Game[]> {
        return await this.ormRepository.find({});
    }

    public async create({ size, colors }: CreateGameDTO): Promise<Game> {
        const game = this.ormRepository.create({ size, colors });
        const board = initializeBoard(size, colors);
        game.moves = Array<string>();
        game.board = board;
        game.optimalMoves = optimalMoves(board);
        await this.ormRepository.save(game);
        return game;
    }

    public async update(game: Game): Promise<Game> {
        await this.ormRepository.save(game);
        return game;
    }

    public async delete(id: number): Promise<void> {
        await this.ormRepository.delete(id);
    }
}
export default GameRepository;
