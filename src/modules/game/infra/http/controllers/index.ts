import { container } from "tsyringe";
import { Request, Response } from "express";
import { GetAllGamesQuery } from "../../../services/queries/GetAllGamesQuery";
import { GetGameDataQuery } from "../../../services/queries/GetGameDataQuery";
import { GameGetDTO } from "../../../dtos/GameGetDTO";
import { CreateGameCommand } from "../../../services/commands/CreateGameCommand";
import { CreateGameDTO } from "../../../dtos/CreateGameDTO";
import { MakeMoveCommand } from "../../../services/commands/MakeMoveCommand";
import { MakeMoveDTO } from "../../../dtos/MakeMoveDTO";

export class GameController {
    public async index(request: Request, response: Response): Promise<Response> {
        const getAllGames = container.resolve(GetAllGamesQuery);
        const games = await getAllGames.execute();
        return response.status(200).json(games);
    }

    public async game(request: Request, response: Response): Promise<Response> {
        const { id } = request.params as GameGetDTO;
        const getGame = container.resolve(GetGameDataQuery);
        const game = await getGame.execute(id!);
        return response.status(200).json(game);
    }

    public async store(request: Request, response: Response): Promise<Response> {
        const { size, colors } = request.body as CreateGameDTO;
        const createGame = container.resolve(CreateGameCommand);
        const game = await createGame.execute({ size, colors });
        return response.status(201).json(game);
    }

    public async update(request: Request, response: Response): Promise<Response> {
        const id = parseInt(request.params.id);
        const { moves } = request.body as MakeMoveDTO;
        const updateGame = container.resolve(MakeMoveCommand);
        const game = await updateGame.execute({ id, moves });
        return response.status(200).json(game);
    }
}
