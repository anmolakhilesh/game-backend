import { celebrate, Joi, Segments } from "celebrate";
import { NextFunction, RequestHandler, Request, Response } from "express";

export default (request: Request, response: Response, next: NextFunction): RequestHandler => {
    return celebrate(
        {
            [Segments.PARAMS]: Joi.object().keys({
                id: Joi.number().positive().required(),
            }),

            [Segments.BODY]: Joi.object().keys({
                moves: Joi.array().items(Joi.string().required()),
            }),
        },
        { abortEarly: false, allowUnknown: false },
    )(request, response, next);
};
