import { celebrate, Joi, Segments } from "celebrate";
import { NextFunction, RequestHandler, Request, Response } from "express";

export default (request: Request, response: Response, next: NextFunction): RequestHandler => {
    return celebrate(
        {
            [Segments.BODY]: Joi.object().keys({
                size: Joi.number().min(2).max(10).positive().required(),
                colors: Joi.number().min(2).max(9).positive().required(),
            }),
        },
        { abortEarly: false, allowUnknown: false },
    )(request, response, next);
};
