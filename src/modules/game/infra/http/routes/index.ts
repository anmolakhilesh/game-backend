import { Router } from "express";
import { GameController } from "../controllers";
import GameGetValidator from "../validators/GameGetValidator";
import GameCreateValidator from "../validators/GameCreateValidator";
import GameUpdateValidator from "../validators/GameUpdateValidator";

const routes = Router();
const gameController = new GameController();

routes.get("/", gameController.index);
routes.post("/", GameCreateValidator, gameController.store);
routes.get("/:id", GameGetValidator, gameController.game);
routes.post("/:id", GameUpdateValidator, gameController.update);

export default routes;
