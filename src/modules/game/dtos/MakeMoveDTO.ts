export interface MakeMoveDTO {
    id: number;
    moves: string[];
}
