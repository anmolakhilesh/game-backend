import { inject, injectable } from "tsyringe";
import { Game } from "../../infra/typeorm/entity";
import { IGameRepository } from "../../repositories/IGameRepository";

@injectable()
export class GetAllGamesQuery {
    constructor(@inject("GameRepository") private gameRepository: IGameRepository) {}
    public async execute(): Promise<Game[]> {
        return await this.gameRepository.findAll();
    }
}
