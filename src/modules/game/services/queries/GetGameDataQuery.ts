import { inject, injectable } from "tsyringe";
import AppError from "../../../../shared/errors/AppError";
import { Game } from "../../infra/typeorm/entity";
import { IGameRepository } from "../../repositories/IGameRepository";

@injectable()
export class GetGameDataQuery {
    constructor(@inject("GameRepository") private gameRepository: IGameRepository) {}
    public async execute(id: number): Promise<Game | undefined> {
        const game = await this.gameRepository.findById(id);
        if (!game) {
            throw new AppError("Game not found", 401);
        }
        return game;
    }
}
