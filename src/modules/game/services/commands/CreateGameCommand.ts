import { inject, injectable } from "tsyringe";
import { CreateGameDTO } from "../../dtos/CreateGameDTO";
import { Game } from "../../infra/typeorm/entity";
import { IGameRepository } from "../../repositories/IGameRepository";

@injectable()
export class CreateGameCommand {
    constructor(@inject("GameRepository") private gameRepository: IGameRepository) {}
    public async execute({ size, colors }: CreateGameDTO): Promise<Game> {
        const game = await this.gameRepository.create({ size, colors });
        return game;
    }
}
