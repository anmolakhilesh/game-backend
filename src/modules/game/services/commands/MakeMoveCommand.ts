import { inject, injectable } from "tsyringe";
import { MakeMoveDTO } from "../../dtos/MakeMoveDTO";
import { Game } from "../../infra/typeorm/entity";
import { IGameRepository } from "../../repositories/IGameRepository";

@injectable()
export class MakeMoveCommand {
    constructor(@inject("GameRepository") private gameRepository: IGameRepository) {}
    public async execute({ id, moves }: MakeMoveDTO): Promise<Game> {
        const game = await this.gameRepository.update({ id, moves });
        return game;
    }
}
