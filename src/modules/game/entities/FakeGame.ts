import { IGame } from "./IGame";

class FakeGame implements IGame {
    id: number;
    size: number;
    colors: number;
    moves: string[];
    optimalMoves: string[];
    board: object;
    createdAt: Date;
    updatedAt: Date;
}
export default FakeGame;
