export interface IGame {
    id: number;
    size: number;
    colors: number;
    moves: string[];
    optimalMoves: string[];
    board: object;
    createdAt: Date;
    updatedAt: Date;
}
