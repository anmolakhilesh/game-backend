import { enumKeys } from "../controllers/utils/enumKeys";

export enum COLOR {
    Pink = "pink",
    Red = "red",
    Blue = "blue",
    Yellow = "yellow",
    Black = "black",
    Green = "green",
    Purple = "purple",
    Grey = "grey",
    Orange = "orange",
}

export const colors = enumKeys<COLOR>(COLOR);
