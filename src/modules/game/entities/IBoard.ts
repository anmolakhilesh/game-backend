import { COLOR } from "./IColors";

export interface iTile {
    isOrigin: boolean;
    color: COLOR;
    row: number;
    colomn: number;
}

export interface iBoard {
    tiles: Tile[][];
    totalColors: COLOR[];
}
