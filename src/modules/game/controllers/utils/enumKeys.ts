export const enumKeys = <T>(value: any): T[] => Object.keys(value).map((key) => value[key]);
