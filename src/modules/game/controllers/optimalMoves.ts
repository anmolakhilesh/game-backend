// import { cloneDeep } from "lodash";
import { COLOR } from "../entities/IColors";
import { iBoard, iTile } from "../entities/IBoard";

export function shallow<T extends object>(source: T): T {
    return Object.assign({}, source);
}

export function deepObject<T>(source: T): T {
    if (typeof source !== "object" || source === null) {
        return source;
    }
    if (Array.isArray(source)) {
        return (source as any).map((value: any) => deepObject(value));
    }
    const shallowCopy = shallow(source as any);
    Object.keys(shallowCopy).map((key) => {
        shallowCopy[key] = deepObject(shallowCopy[key]);
    });
    return shallowCopy;
}

const NORTH_SOUTH_EAST_WEST = [
    [-1, 0],
    [0, -1],
    [0, 1],
    [1, 0],
];

export const getAdjacentTiles = (board: iBoard, tile: iTile) => {
    return NORTH_SOUTH_EAST_WEST.map((indicesToAdd) => {
        const row = board.tiles[tile.row + indicesToAdd[0]];
        if (!row) {
            return undefined;
        }
        return row[tile.colomn + indicesToAdd[1]];
    }).filter((tile) => !!tile) as iTile[];
};

type Covered = {
    [key in string]: boolean;
};

const getVisitedKey = (tile: iTile) => `${tile.row},${tile.colomn}`;

type ConnectedTilesResult = {
    connectedTiles: iTile[];
    covered: Covered;
};

export const getConnectedTiles = (
    board: iBoard,
    tile: iTile,
    colorToMatch: COLOR,
    covered: Covered,
): ConnectedTilesResult => {
    const visitedKey = getVisitedKey(tile);

    if (covered[visitedKey]) {
        return {
            connectedTiles: [],
            covered,
        };
    }

    let updatedVisited = {
        ...covered,
        [visitedKey]: true,
    };

    if (tile.color !== colorToMatch) {
        return {
            connectedTiles: [],
            covered: updatedVisited,
        };
    }

    const adjacentTiles = getAdjacentTiles(board, tile);

    return adjacentTiles.reduce<ConnectedTilesResult>(
        (acc, curr) => {
            const { connectedTiles, covered } = getConnectedTiles(board, curr, colorToMatch, acc.covered);

            return {
                connectedTiles: acc.connectedTiles.concat(connectedTiles),
                covered,
            };
        },
        {
            connectedTiles: [tile],
            covered: updatedVisited,
        },
    );
};

export const getColor = (board: iBoard, color: COLOR) => {
    const updatedBoard = deepObject(board);

    const start = updatedBoard.tiles[0][0];
    const { connectedTiles } = getConnectedTiles(updatedBoard, start, start.color, {});

    connectedTiles.forEach((tile) => {
        tile.color = color;
    });

    return updatedBoard;
};

export const getNumberOfNeighbours = (board: iBoard) => {
    const start = board.tiles[0][0];
    return getConnectedTiles(board, start, start.color, {}).connectedTiles.length;
};

export const optimalMoves = (board: iBoard) => {
    const end = (board: iBoard) => {
        return getNumberOfNeighbours(board) === Math.pow(board.tiles[0].length, 2);
    };

    const getAllNeighbours = (board: iBoard) => {
        return board.totalColors
            .map((color) => {
                return {
                    color,
                    possibleConnectedTiles: getNumberOfNeighbours(getColor(board, color)),
                };
            })
            .sort((a, b) => b.possibleConnectedTiles - a.possibleConnectedTiles)[0];
    };

    const colors: COLOR[] = [];

    while (!end(board)) {
        const bestColor = getAllNeighbours(board).color;
        board = getColor(board, bestColor);
        colors.push(bestColor);
    }

    return colors;
};
