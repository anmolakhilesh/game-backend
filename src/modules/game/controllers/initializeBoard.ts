import { colors } from "../entities/IColors";
import { iTile } from "../entities/IBoard";

export const initializeBoard = (colomnsSize: number, colorsCount: number) => {
    const getColors = colors.slice(0, colorsCount);

    const board = Array.from({ length: colomnsSize }).map((_, rowNo) => {
        return Array.from({ length: colomnsSize }).map((_, colomnNo) => {
            return {
                color: getColors[Math.floor(Math.random() * getColors.length)],
                isOrigin: rowNo + colomnNo === 0,
                colomn: colomnNo,
                row: rowNo,
            } as iTile;
        });
    });
    return {
        tiles: board,
        totalColors: getColors,
    };
};
