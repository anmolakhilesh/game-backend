import { CreateGameDTO } from "../dtos/CreateGameDTO";
import { MakeMoveDTO } from "../dtos/MakeMoveDTO";
import { IGame } from "../entities/IGame";
import { IMove } from "../entities/IMove";

export interface IGameRepository {
    findById(id: number): Promise<IGame | undefined>;
    findAll(): Promise<IGame[]>;
    create(data: CreateGameDTO): Promise<IGame>;
    update(data: MakeMoveDTO): Promise<IMove>;
    delete(id: number): Promise<void>;
}
