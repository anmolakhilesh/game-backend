import { IMove } from "./../entities/IMove";
import { CreateGameDTO } from "../dtos/CreateGameDTO";
import { MakeMoveDTO } from "../dtos/MakeMoveDTO";
import FakeGame from "../entities/FakeGame";
import { IGame } from "../entities/IGame";
import { IGameRepository } from "./IGameRepository";

export class FakeGameRepository implements IGameRepository {
    private games: FakeGame[] = [];

    public async findById(id: number) {
        const game = this.games.find((game) => game.id === id);
        return game;
    }

    public async findAll(): Promise<IGame[]> {
        return this.games;
    }

    public async create({ size, colors }: CreateGameDTO): Promise<IGame> {
        const game = new FakeGame();
        game.id = Math.floor(Math.random() * 100);
        game.size = size;
        game.colors = colors;
        game.moves = [];
        game.optimalMoves = [];
        game.board = {};
        game.createdAt = new Date();
        game.updatedAt = new Date();

        this.games.push(game);
        return game;
    }

    public async update({ id, moves }: MakeMoveDTO): Promise<IMove> {
        const findGame = this.games.findIndex((game) => game.id === id);
        const game = this.games[findGame];
        game.moves = moves;
        return game;
    }

    public async delete(id: number): Promise<void> {
        this.games = this.games.filter((game) => game.id !== id);
    }
}
