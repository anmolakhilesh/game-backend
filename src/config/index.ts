import "dotenv/config";

interface Config {
    dbName: string;
    dbUser: string;
    dbPassword: string;
}

export default {
    dbName: process.env.POSTGRES_DB,
    dbUser: process.env.POSTGRES_USER,
    dbPassword: process.env.POSTGRES_PASSWORD,
} as Config;
