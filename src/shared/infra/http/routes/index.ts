import { Router } from "express";
import gameRouter from "../../../../modules/game/infra/http/routes";

const routes = Router();

routes.use("/game", gameRouter);

export default routes;
