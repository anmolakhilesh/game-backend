import GameRepository from "../../modules/game/infra/typeorm/repository";
import { container } from "tsyringe";
import { IGameRepository } from "../../modules/game/repositories/IGameRepository";

container.registerSingleton<IGameRepository>("GameRepository", GameRepository);
